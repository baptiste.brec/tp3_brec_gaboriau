import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_digits
from matplotlib import pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay
from sklearn.model_selection import validation_curve





#QUESTION 3 :

digits = load_digits()
X = digits.data
y = digits.target
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2,train_size = 0.8, random_state=1)

acc_train_mean = []
acc_train_std = []

acc_validation_mean = []
acc_validation_std = []

C = np.array([1,2,3,4,5,8,10,15,20,25,30,35,40])

train_scores, validation_scores = validation_curve(
        MLPClassifier(activation='tanh', solver='adam', batch_size=1,alpha=0, learning_rate='adaptive',early_stopping=True, validation_fraction=0.2, verbose =(1)), 
        X_train, y_train, param_name="hidden_layer_sizes",param_range=C,scoring="accuracy", n_jobs=2)

#matrice de confusion
# from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay
# from sklearn.datasets import make_classification

# y_pred_test = clf2.predict(X_test)
# confusion_matrix = confusion_matrix(y_test, y_pred_test)

# mat_conf = ConfusionMatrixDisplay(confusion_matrix=confusion_matrix)
# mat_conf.plot()
# plt.show()

# plt.plot(train_scores)
# mean = np.mean(train_scores,axis=1)
# std = np.std(train_scores,axis=1)
# plt.errorbar(C,mean,std)

# one_NN = KNeighborsClassifier(n_neighbors= 1, algorithm='brute')
# one_NN.fit(X_train, y_train)

# #score sur la base d'apprentissage
# acc_train_set = one_NN.score(X_train, y_train)
# #score sur la base de test
# acc_test_set =  one_NN.score(X_test, y_test)







#QUESTION 4 : 

print(train_scores)

def argmax_reject_threshold(y, threshold):
    y_argmax = np.argmax(y, axis=1)
    y_masked = np.ma.array(y_argmax, mask=(np.amax(y, axis=1) < threshold))

    return y_masked.filled(-1)

def argmax_top2_reject_threshold(y, threshold):
    y_argmax = np.argmax(y, axis=1)
    y_top2 = np.sort(y, axis=1)[:,-1:-3:-1]
    y_masked = np.ma.array(y_argmax, mask=((y_top2[:,0] - y_top2[:,1]) < threshold))

    return y_masked.filled(-1)

def common_value(array1,array2):
    num_matches = 0
    for x, y in zip(array1, array2):
        if x == y:
            num_matches += 1
    
    return num_matches

distance_rej=[]
distance_rec=[]
distance_eff=[]
ambiguite_rej=[]
ambiguite_rec=[]
ambiguite_eff=[]

for i in range(15):
    
    rejet_par_distance=[]
    rejet_par_ambiguite=[]
    taux_rejet_distance = []
    taux_reconnaissance_distance = []
    distance_efficacite=[]
    taux_rejet_ambiguite = []
    taux_reconnaissance_ambiguite = []
    ambiguous_efficacite=[]

    clf4 = MLPClassifier(hidden_layer_sizes=15, activation='tanh', solver='adam', batch_size=1, alpha=0, learning_rate='adaptive', verbose=1)
    clf4.fit(X_train,y_train)
    y_test_probabilite = clf4.predict_proba(X_test)
    parametre_treshold = 0
    
    for i in range(100):
        #tableaux de rejet
        rejet_par_distance.append(argmax_reject_threshold(y_test_probabilite,i/1e2))
        rejet_par_ambiguite.append(argmax_top2_reject_threshold(y_test_probabilite,i/1e2))
         
        #calcul des taux rejet par distance
        taux_rejet_distance.append(( np.count_nonzero(rejet_par_distance[i]==-1)/len(X_test)))
        taux_reconnaissance_distance.append(common_value(rejet_par_distance[i],y_test)/(len(X_test)- np.count_nonzero(rejet_par_distance[i]==-1)))
        #efficacité
        if (np.count_nonzero(rejet_par_distance[i]==-1)==0):
            distance_efficacite.append(0)
        else:
            distance_efficacite.append(common_value(rejet_par_distance[i],y_test)/np.count_nonzero(rejet_par_distance[i]==-1))

        #calcul des taux de rejet par ambiguité
        taux_rejet_ambiguite.append((np.count_nonzero(rejet_par_ambiguite[i]==-1)/len(X_test)))
        taux_reconnaissance_ambiguite.append(common_value(rejet_par_ambiguite[i],y_test)/(len(X_test)-np.count_nonzero(rejet_par_ambiguite[i]==-1)))
        #efficacité 
        if (np.count_nonzero(rejet_par_ambiguite[i]==-1)==0):
            ambiguous_efficacite.append(0)
        else:
            ambiguous_efficacite.append(common_value(rejet_par_ambiguite[i],y_test)/np.count_nonzero(rejet_par_ambiguite[i]==-1))
    
    #distance
    distance_rej.append(taux_rejet_distance)
    distance_rec.append(taux_reconnaissance_distance)
    distance_eff.append(distance_efficacite)
    #ambiguité
    ambiguite_rej.append(taux_rejet_ambiguite)
    ambiguite_rec.append(taux_reconnaissance_ambiguite)
    ambiguite_eff.append(ambiguous_efficacite)

#Moyenne des taux
nouv_distance_rec=np.mean(distance_rec,axis=0)
nouv_distance_rej=np.mean(distance_rej,axis=0)
nouv_distance_eff=np.mean(distance_eff,axis=0)
nouv_ambiguite_rec=np.mean(ambiguite_rec,axis=0)
nouv_ambiguite_rej=np.mean(ambiguite_rej,axis=0)
nouv_ambiguite_eff=np.mean(ambiguite_eff,axis=0)

#Courbe ; taux de reconnaissance en fonction du taux de rejet, utilisation du rejet par distance
plt.plot(nouv_distance_rej,nouv_distance_rec,label="Distance")
plt.plot(nouv_ambiguite_rej,nouv_ambiguite_rec,label="Ambiguité")

plt.xlabel("taux de  rejet")
plt.ylabel("taux de reconnaissance")
plt.title('Rejet par distance')
plt.legend()
plt.grid()
plt.show()






#QUESTION 5 :

#Prise du rejet par distance
clf4 = MLPClassifier(hidden_layer_sizes=15, activation='tanh', solver='adam', batch_size=1, alpha=0, learning_rate='adaptive', verbose=1)
clf4.fit(X_train,y_train)
One_NN = KNeighborsClassifier(n_neighbors=7, algorithm='brute')
One_NN.fit(X_train, y_train)
y_test_probabilite = clf4.predict_proba(X_test)
#treeshold = 0.4
liste_etage=argmax_reject_threshold(y_test_probabilite,0.4)
liste_cascade=[]

for index,value in enumerate(liste_etage):
    if value==-1:
        liste_cascade.append(One_NN.predict(X_test)[index])
        print("Changement du n°",index,"qui avait",value,"en",liste_cascade[index])
    else:
        liste_cascade.append(value)

print("Taux d'erreur' :",(1-common_value(liste_cascade,y_test)/len(y_test))*100)

confusion_etage = confusion_matrix(y_test, liste_etage)
mat_conf = ConfusionMatrixDisplay(confusion_matrix=confusion_etage)
mat_conf.plot()
plt.show()
confusion_cascade = confusion_matrix(y_test, liste_cascade)
mat_conf = ConfusionMatrixDisplay(confusion_matrix=confusion_cascade)
mat_conf.plot()
plt.show()
